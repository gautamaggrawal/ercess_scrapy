#!/usr/bin/python
# -*- coding: utf-8 -*-
import scrapy

from ercessscrapper.items import ErcessscrapperItem

from scrapy.http.request import Request


# crawler to scrape only some webpages

class ErcessJobSpider(scrapy.Spider):

    name = 'ercess'

    allowed_domains = ['ercess.com']
    start_urls = ['https://www.ercess.com/live/']

    def parse(self, response):

        # how to extract data

        hrefs = response.css('a::attr(href)').extract()

        for item in zip(hrefs):

            new_item = ErcessscrapperItem()

            new_item['link'] = item[0]

            yield new_item

        nextpage_list = [
            'https://ercess.com/live/welcome/mumbai',
            'https://ercess.com/live/welcome/delhi',
            'https://ercess.com/live/welcome/chennai',
            'https://ercess.com/live/welcome/bangalore',
            'https://ercess.com/live/welcome/kolkata',
            'https://ercess.com/live/welcome/pune',
            'https://ercess.com/live/welcome/hyderabad',
            ]

        for nurl in nextpage_list:
            yield Request(url=nurl, callback=self.parse)


# Spider for extracts site for complete website

class ExtractUrls(scrapy.Spider):

    name = 'extract'

    # request function

    def start_requests(self):
        urls = ['http://www.ercess.com']

        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    # Parse function

    def parse(self, response):

        # Extra feature to get title

        title = response.css('title::text').extract_first()

        # Get anchor tags

        links = response.css('a::attr(href)').extract()

        for item in zip(links):

            new_item = ErcessscrapperItem()

            new_item['link'] = item[0]

            yield new_item

        for link in links:
            yield {
                'title': title,
                'links': link
            }

            if 'ercess' in link:
                yield scrapy.Request(url=link, callback=self.parse)
